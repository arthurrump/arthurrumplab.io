+++
title = "Requirements for an Automated Assessment Tool for Learning Programming by Doing"
tagline = "Conference paper for ICST 2025 - with Vadim Zaytsev and Angelika Mader"
tech = []
tags = [ "Atelier", "Education", "Programming Education", "Static Analysis" ]
color = "#604ebf"
order = 5

[[links]]
name = "Paper (local copy)"
icon = "document-outline"
url = "/research/requirements_icst2025/paper.pdf"
[[links]]
name = "Companion package (Zenodo)"
icon = "zenodo"
url = "https://doi.org/10.5281/zenodo.14627201"
+++

Assessment of open-ended assignments such as programming projects is a complex and time-consuming task. When students learn to program, however, they benefit from receiving timely feedback, which requires an assessment of their current work. Our goal is to build a tool that assists in this process by partially automating the assessment of open-ended programming assignments. In this paper we discuss the requirements for this tool, based on interviews with teachers and other relevant stakeholders.
